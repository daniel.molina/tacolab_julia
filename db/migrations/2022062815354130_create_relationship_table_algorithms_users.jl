module CreateRelationshipTableAlgorithmsUsers

import SearchLight.Migrations: create_table, column, columns, pk, add_index, drop_table

function up()
  create_table(:algorithmsusers) do
    [
      pk()
      column(:algorithms_id, :int)
      column(:users_id, :int)
    ]
  end

  add_index(:algorithmsusers, :algorithms_id)
  add_index(:algorithmsusers, :users_id)
end

function down()
  drop_table(:algorithmsusers)
end

end
