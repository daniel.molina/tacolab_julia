# Toolkit for Automatic Comparison of Optimisers: TACO

Website to automatically compare results of algorithms for real and large-scale
global optimization. Initially it was designed by the [Task Force on Large Scale
Global Optimization](http://tflsgo.org/) for its own competitions, like
for [WCCI'2018](http://tflsgo.org/special_sessions/cec2018.html).

However, it can be used for each Benchmark competition. The idea of the website
is to allow researchers to compare the results of its algorithms compared with
other algorithms.

## Demo


## Julia Version

This project is an adaptation of tacolab from
https://gitlab.com/daniel.molina/tacolab, it presents the main following
differences:

- It uses Julia using [Genie](https://www.genieframework.com/), instead of
  Python with Flask.

- It uses
  [GenieAuthentication](https://github.com/GenieFramework/GenieAuthentication.jl)
  to authenticate users.

- It uses [htmlx](https://htmx.org/) to avoid using Vue and JavaScript in general.

- Using PyPlots instead of Brokeh.

I have changed the project in order to have a simpler code and easier to
maintain.

## Requirements

It requires several libraries, indicated in requirements. You can install the
required libraries (in a virtual environment) with:

```shell
julia --project=. -e 'using Pkg; Pkg.instantiate()'
```

## Init the database

At the beginning, the app create the initial database, with the CEC'2013
Benchmark. No data algorithms is inserted.

## Run the application

You can download the app in a directory and then to run it:

```sh
$ bin/server
```

By default it runs in development mode. For a real production mode, you should
serve the static files by a webserver like Apache, Nginx, ...

Also, you should define:

```sh
$ export GENIE_ENV=prod
$ bin/server
```

## Roadmap

- [ ] Add description of algorithms.

- [ ] Allow users to identify.

- [ ] Allow admin to create benchmarks using the web interface.

- Using private algorithms results.

## Testing the website

To compare, it is required to use a Excel file. In examples/ there are several
examples to test it.

## Structure

This project follows the Genie structure. The main files are:

- routes.jl: Routes for the website.

- app/layouts/tacolab.jl.html: Global html template.

- templates/*.html: Templates for the different website.
   - index.html: Index page.
   - bench.html: website for the standard benchmarks.

- app/resources/benchmarks/: The benchmark interface. It is composed by:

  - Benchmarks.jl: Structure benchmark with the methods to access and modify the
    benchmark information.

  - BenchmarksController.jl: Methods used in routes.jl.
