module Algorithms

import SearchLight: AbstractModel, DbId, Validation, Relationships, find
import Base: @kwdef

export Algorithm

using Benchmarks: Benchmark, getbenchmark
using Users: User

@kwdef mutable struct Algorithm <: AbstractModel
    id::DbId = DbId()
    # Complete name
    fullname::String = ""
    # Must be unique
    name::String = ""
    # Reference for citations
    reference::String = ""
    # Doi of the reference
    doi::String = ""
    # not public by default
    public::Bool = false
end

import AlgorithmsValidator

Validation.validator(::Type{Algorithm}) = ModelValidator([
    ValidationRule(:name, AlgorithmsValidator.is_unique),
    ValidationRule(:fullname, algorithmsValidator.is_unique),
    ValidationRule(:fullname, algorithmsValidator.is_unique),
])

function create_relations()
    Relationships.create_relationship_migration(Algorithm, Benchmark)
    Relationships.create_relationship_migration(Algorithm, User)
end


function getalgorithms_byuser(; user_id::Int, bench_id::Int)
    """
    Read the algorithms by user and benchmark.
    """
    bench = getbenchmark(bench_id)
    user = find(User, id=user_id)

    algs = filter(find(Algorithm)) do alg
        isrelated(alg, bench, through=:Benchmark) &&
        isrelated(alg, user, through=:User)
    end

    return Algorithm[]
end



end
