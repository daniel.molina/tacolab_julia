using Genie.Router
using Genie.Renderer
using Genie.Renderer.Html
using Genie.Renderer.Json

# Add Authentication
using GenieAuthentication
using GenieAuthorisation

using BenchmarksController: showbenchmark
using Benchmarks: getbenchmarks, getdimensions, getreports
using AlgorithmsController: showmyalgorithms

const layout=path"app/layouts/tacolab.jl.html"
const error=""

route("/", named=:home) do
  html(path"templates/index.html", layout=layout, context=@__MODULE__)
end

route("/comp", named=:generic) do
    html(path"templates/comp.html", layout=layout, reports=getreports(), context=@__MODULE__)
end

route("/bench", named=:benchs) do
    html(path"templates/bench.html", layout=layout, benchmarks=getbenchmarks(), context=@__MODULE__)
end

route("/compared", named=:compare, method=POST) do
    # this code is only accessible for authenticated users
    html("TODO", layout=layout, context=@__MODULE__)
end

route("/private", named=:private) do; @authenticated!
    # this code is only accessible for authenticated users
    html(path"templates/private.html", layout=layout, benchmarks=getbenchmarks(), context=@__MODULE__)
end

# TODO: List the algorithms for current user
route("/myalgs/", named=:get_algs) do; @authenticated!
    return showmyalgorithms()
end

route(showbenchmark, "/benchinfo")
