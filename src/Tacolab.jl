module Tacolab

using Genie, Logging, LoggingExtras
using GenieAutoReload

# Enable autoreload
Genie.config.websockets_server = true
GenieAutoReload.autoreload(pwd())

function main()
  Base.eval(Main, :(const UserApp = Tacolab))

  Genie.genie(; context = @__MODULE__)

  Base.eval(Main, :(const Genie = Tacolab.Genie))
  Base.eval(Main, :(using Genie))
end

end
