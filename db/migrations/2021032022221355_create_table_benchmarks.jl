module CreateTableBenchmarks

import SearchLight.Migrations: create_table, column, primary_key, add_index, drop_table

function up()
  create_table(:benchmarks) do
    [
        primary_key()
        # Name must be unique
        column(:name, :string, limit=30, "UNIQUE")
        # Title
        column(:title, :string, limit=50)
        # Description
        column(:description, :string, limit=1_000)
        # Number of functions
        column(:nfuns, :int)
        # Removed functions
        column(:removed, :string)
        # Categories (join by ,)
        column(:categories, :string, limit=30)
        # Tablenames
        # column(:tablename, :string, limit=30)
    ]
  end

  add_index(:benchmarks, :name)
end

function down()
  drop_table(:benchmarks)
end

end
