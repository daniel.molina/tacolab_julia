
module AlgorithmsController

using Algorithms

using Genie.Renderer
using Genie.Renderer.Html

using Genie.Router

using GenieAuthentication

import SearchLight: findone
using Algorithms: Algorithm, getalgorithms_byuser

using Benchmarks: getbenchmark

function showmyalgorithms()
    """
    Return the current algorithms considering both the userid and the benchmarkid
    """
    args = params()
    @show args

    if !haskey(args, :benchmark_id)
        return
    end

    bench_id = tryparse(Int, args[:benchmark_id])

    if isnothing(bench_id) || bench_id < 0
        return
    end

    if !is_authenticated()
        return
    end

    bench = getbenchmark(bench_id)
    user_id = get_authentication()
    algs = getalgorithms_byuser(; user_id, bench_id)
    return html(path"templates/private_algs.jl.html", layout = nothing; bench, algs)
end

end
