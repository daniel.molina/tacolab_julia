module BenchmarksController

using Genie.Renderer
using Genie.Renderer.Html

using Benchmarks: getbenchmarks, getbenchmark, getdimensions
using Genie.Router

function benchmarks()
    # return json(getbenchmarks())
end

function showbenchmark()
    if !haskey(params(), :benchmark_id)
        return
    end

    benchmark_id = params(:benchmark_id)
    id_int = tryparse(Int, benchmark_id)

    if isnothing(id_int) || id_int < 0
        return
    end

    bench = getbenchmark(benchmark_id)
    dimensions = getdimensions(bench)
    return html(path"templates/benchinfo.jl.html", layout=nothing, bench=bench, categories=dimensions)
end

end
