module CreateRelationshipTableAlgorithmsBenchmarks

import SearchLight.Migrations: create_table, column, columns, pk, add_index, drop_table

function up()
  create_table(:algorithmsbenchmarks) do
    [
      pk()
      column(:algorithms_id, :int)
      column(:benchmarks_id, :int)
    ]
  end

  add_index(:algorithmsbenchmarks, :algorithms_id)
  add_index(:algorithmsbenchmarks, :benchmarks_id)
end

function down()
  drop_table(:algorithmsbenchmarks)
end

end
