module Benchmarks

using Genie
import SearchLight: AbstractModel, DbId, save, findone, find, SQLOrder
import YAML
import Markdown
import Base: @kwdef

export Benchmark

@kwdef mutable struct Benchmark <: AbstractModel
    id::DbId = DbId()
    # Must be unique
    name::String = "Benchmark"
    title::String = ""
    # Description
    description::String = ""
    # Number of nfuns
    nfuns::Int=0
    removed::String=""
    categories::String=""
end

function seed()
    benchmarks_yaml = YAML.load_file(joinpath(Genie.config.path_resources, "benchmarks", "default.yml"))

    for bench in benchmarks_yaml["benchmarks"]
        @show bench
        removed = removed=isnothing(bench["removed"]) ? "" : string(bench["removed"])
        b = Benchmark(name=bench["name"], title=bench["title"], description=bench["description"], nfuns=bench["nfuns"], categories=string(bench["categories"]), removed=removed)
        @show b
        save(b, skip_validation = true)
    end
end

function getbenchmarks()
    # Add the default values
    if count(Benchmark) == 0
        seed()
    end
    return find(Benchmark, order=SQLOrder(:name))
end

function getbenchmark(id)
    return findone(Benchmark, id=id)
end

function getdimensions(bench)
    cats = bench.categories
    return parse.(Int, split(cats, ","))
end

function getreports()
    return [(id="mean", title="Mean Comparison"),
            (id="ranking", title="Ranking Comparison"),
            (id="np-test", title="Non-Parametric Tests")]
end

end
