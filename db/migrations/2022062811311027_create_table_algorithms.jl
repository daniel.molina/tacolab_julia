module CreateTableAlgorithms

import SearchLight.Migrations: create_table, column, columns, pk, add_index, drop_table, add_indices

function up()
    create_table(:algorithms) do
        [
            pk()
            column(:fullname, :string, limit = 50, "UNIQUE")
            column(:name, :string, limit = 15, "UNIQUE")
            column(:reference, :string, limit = 150)
            column(:doi, :string, limit = 30)
            column(:public, :bool, default=false)
        ]
    end

    add_index(:algorithms, :name)
end

function down()
  drop_table(:algorithms)
end

end
